const bannerParallax = (el) => {
  window.addEventListener('scroll', function(){
    const y = this.scrollY;
    const f = y * .3;
    el.style.transform = `translateY(${f}px)`
  })
}
const bannerImg = document.querySelector('.banner img.main-img');
if(bannerImg){
  bannerParallax(bannerImg)
}