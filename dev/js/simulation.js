const simulationInit = async () => {
  const response = await fetch('/assets/price.json');
  if (response.ok) {
    const json = await response.json();
    Object.keys(json[0]).map(i => {
      const option = `<option value='${i}'>${i}</option>`
      const from = document.querySelector('select#simulation-from');
      const to = document.querySelector('select#simulation-to');
      from.insertAdjacentHTML('beforeend', option);
      to.insertAdjacentHTML('beforeend', option);
    })
  }
}

const calculateSimulation = async () => {
  const response = await fetch('/assets/price.json');
  const from = document.querySelector('select#simulation-from');
  const to = document.querySelector('select#simulation-to');
  const type = document.querySelector('input[name="golongan"]:checked');
  // const type = document.querySelector('select#simulation-type')
  if (response.ok) {
    const json = await response.json();
    const data = json[0];
    let out;
    if (from.value !== to.value) {
      out = data[from.value][to.value][type.value];
    } else {
      out = 0;
    }
    const result = out.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, '$&,')
    const resultContainer = document.getElementById('resultSimulation');
    resultContainer.closest('h1').classList.add('val')
    resultContainer.innerHTML = result;
  }
}

const simulationButton = document.querySelector('button#calculateSimulation');
if(simulationButton){
  simulationButton.addEventListener('click', () => calculateSimulation());
}

const simulationType = document.querySelector('select#simulation-type');
const simulationTypeChange = (e) => {
  let html;
  const typeContainer = document.querySelector('.golongan--container');
  if (e.value === "1") {
    html = `
      <div class="golongan--item">
        <p class="bold info">Golongan I:</p>
        <img src="/tmj-dev/assets/images/golongan/golongan_1.png" alt="">
        <p class="info noMargin">(Sedan, Jip, Pick Up/Truk Kecil, dan Bus)</p>
      </div>
    `
  } else if (e.value === "2") {
    html = `
    <div class="golongan--item">
      <p class="bold info">Golongan II:</p>
      <img src="/tmj-dev/assets/images/golongan/golongan_2.png" alt="">
      <p class="info noMargin">(Truk dengan 2 (dua) gandar)</p>
    </div>
  `
  } else if (e.value === "3") {
    html = `
      <div class="golongan--item">
        <p class="bold info">Golongan III:</p>
        <img src="/tmj-dev/assets/images/golongan/golongan_3.png" alt="">
        <p class="info noMargin">(Truk dengan 3 (tiga) gandar)</p>
      </div>
    `
  } else if (e.value === "4") {
    html = `
      <div class="golongan--item">
        <p class="bold info">Golongan IV:</p>
        <img src="/tmj-dev/assets/images/golongan/golongan_4.png" alt="">
        <p class="info noMargin">(Truk dengan 4 (empat) gandar)</p>
      </div>
    `
  } else if (e.value === "5") {
    html = `
      <div class="golongan--item">
        <p class="bold info">Golongan V:</p>
        <img src="/tmj-dev/assets/images/golongan/golongan_5.png" alt="">
        <p class="info noMargin">(Truk dengan 5 (lima) gandar)</p>
      </div>
    `
  }
  typeContainer.innerHTML = html;
}
if(simulationType){
  simulationType.addEventListener('change', () => simulationTypeChange(simulationType));
}