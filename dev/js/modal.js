const modal = () => {
  const el = document.querySelectorAll('[data-modal-target]');
  const open = function(el){
    const id = el.getAttribute('data-modal-target');
    const target = document.querySelector(`.modal[data-modal='${id}']`);
    target.classList.add('active')
  }
  for(let item of el){
    item.addEventListener('click', () => open(item))
  }
}
const closeModal = (e) => {
  const target = e.closest('.modal[data-modal]');
  target.classList.remove('active');
} 
modal();