const subHeader = () => {
  const subEl = document.querySelector('.bannerHeader .subMenu');
  if(subEl){
    const header = document.querySelector('.header').offsetHeight;
    const bannerHeader = document.querySelector('.bannerHeader .banner').offsetHeight;
    const subElHeight = subEl.offsetHeight;
    const app = document.querySelector('.app');
    window.addEventListener('scroll', function(e){
      const scrollTop = document.documentElement.scrollTop;
      if(scrollTop >= bannerHeader){
        subEl.classList.add('fixed')
        app.style.marginTop = `${header + subElHeight}px`
      } else {
        subEl.classList.remove('fixed')
        app.style.marginTop = `${header}px`
      }
    })
  }
}
const menuMobile = document.querySelector('.header--buttonMobile');
const winWidth = window.innerWidth;
if(menuMobile && winWidth <= 768){
  const m = document.querySelector('.header--menu');
  menuMobile.addEventListener('focus', function(){
    m.classList.add('focus')
  })
  document.addEventListener('click', function(e){
    const n = e.target.closest('.header--menu');
    const a = e.target.closest('.header--buttonMobile');
    if(n){
      m.classList.remove('focus')
    } else if (a) {
      m.classList.add('focus')
    } else {
      m.classList.remove('focus')
    }
  })
}

subHeader();