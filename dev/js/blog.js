const floatMenu = () => {
  const menu = document.querySelector('.staticPost .leftMenu');
  const winWidth = window.innerWidth;
  if(menu && winWidth > 768){
    window.addEventListener('scroll', function(){
      const space = 20;
      const header = document.querySelector('.header').offsetHeight;
      const top = menu.getBoundingClientRect().top;
      const count = top - header - space;
      const float = menu.querySelector('.float-menu');
      const limit = menu.getBoundingClientRect().bottom - float.offsetHeight - header - space;


      if(count < 0 && limit > 0){
        float.classList.add('fixed')
        float.classList.remove('absolute')
      } else if (count < 0 && limit < 0) {
        float.classList.remove('fixed')
        float.classList.add('absolute')
      } else { 
        float.classList.remove('fixed')
        float.classList.remove('absolute')
      }
    });
  }
}

floatMenu();